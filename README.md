[TOC]

# vCompute运行库及配置文件



## 算法库及可执行文件
### linux64

```
支持vCompute在linux上位机运行的版本
```

### oe-arm-32
```
支持vCompute在相关arm32硬件上运行的版本
```

## 配置文件

- exd.json

## 更新日志

### 2020年12月26号

- 添加优化算子WLagN、WLeadN（注：WLeadN只能用在列名上）

#### example

  ```
  wlead ('a')         >>>> OK
  wlead ('a + 1')   >>>> OK 
  wleadn('a')         >>>>  OK
  wleadn('a + 1')   >>>> NULL
  ```

效果，CPU占用比33-》25

![image-20201228071032780](README.assets/image-20201228071032780.png)


- 增加base64和string类型的机器学习算法支持，对于硬件的安全性和性能有不同选择

#### [base64](https://en.wikipedia.org/wiki/Base64) support

```
"packages": [
        {
          "content": "...",
          "name": "cc9d77e1-7bbc-4e25-a9a1-cf56a3a0e264",
          "type": "model",
          "encode": "base64"
        }
      ]
```
#### string support

```
"packages": [
        {
          "content": "...",
          "name": "cc9d77e1-7bbc-4e25-a9a1-cf56a3a0e264",
          "type": "model",
          "encode": "string"
        }
      ]
```